package com.yits.allshop.presenter.impl;

import com.yits.allshop.model.ShopDataModel;
import com.yits.allshop.presenter.IShopListPresenter;
import com.yits.allshop.repository.IShopListRepository;
import com.yits.allshop.repository.impl.ShopListRepository;
import com.yits.allshop.utility.AppConstant;
import com.yits.allshop.utility.ConnectionDetector;
import com.yits.allshop.view.activity.ShopListActivity;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by yitsol on 08-02-2018.
 */

public class ShopListPresenter implements IShopListPresenter
{

    private ShopListActivity view;
    private ConnectionDetector connectionDetector;
    private IShopListRepository repository;
    private IShopListRepository.OnGetAllShopDetailsCallback shopDetailsCallback;
    private IShopListRepository.OnDeleteShopCallback deleteShopCallback;

    public ShopListPresenter(ShopListActivity view) {
        this.view = view;
        this.connectionDetector=new ConnectionDetector(view.getContext());
        repository=new ShopListRepository();
    }

    @Override
    public void getAllShopDetails() {
        if(connectionDetector.isConnectedToInternet())
        {
            view.showLoaderNew();
            repository.getAllShopDetails(view.getContext(),shopDetailsCallback);
        }
        else
        {
            view.showMessage(AppConstant.CHECK_NETWORK_CONN);
        }
    }

    @Override
    public void subscribeCallbacks() {
        shopDetailsCallback=new IShopListRepository.OnGetAllShopDetailsCallback() {
            @Override
            public void onSuccess(List<ShopDataModel> detailsModel) {
                view.hideloader();
                view.showAllShopDetails(detailsModel);
            }

            @Override
            public void onError(String message) {
                view.hideloader();
                view.showMessage(message);
            }
        };

        deleteShopCallback=new IShopListRepository.OnDeleteShopCallback() {
            @Override
            public void onSuccess() {
                view.hideloader();
                view.shopDeleteSuccess();
            }

            @Override
            public void onError(String message) {
                view.hideloader();
                view.showMessage(message);
            }
        };
    }

    @Override
    public void unSubscribeCallbacks() {
        shopDetailsCallback=null;
        deleteShopCallback=null;
    }

    @Override
    public void deleteShopFromPosition(@NotNull ShopDataModel datamodel) {
        try {
            if(connectionDetector.isConnectedToInternet())
            {
                view.showLoaderNew();
                repository.deleteShopData(datamodel,deleteShopCallback);
            }
            else
            {
                view.showMessage(AppConstant.CHECK_NETWORK_CONN);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
