package com.yits.allshop.presenter

/**
 * Created by yitsol on 07-02-2018.
 */
interface IShopTypeItemListener
{
    fun onShopTypeClick(id:String, typeId:String,typeName:String)
}