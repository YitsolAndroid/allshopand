package com.yits.allshop.presenter;

/**
 * Created by user on 5/18/2017.
 */

public interface ICaptureImageItemClickListener
{
    void onCaptureImgCamera();
    void onCaptureImgGallery();
}
