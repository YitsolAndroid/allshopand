package com.yits.allshop.presenter.impl;

import com.yits.allshop.model.AppProductImgModel;
import com.yits.allshop.presenter.IAddProductPresenter;
import com.yits.allshop.repository.IAddProductRepository;
import com.yits.allshop.repository.impl.AddProductRepository;
import com.yits.allshop.utility.AppConstant;
import com.yits.allshop.utility.ConnectionDetector;
import com.yits.allshop.view.activity.AddProductActivity;

import java.util.List;

/**
 * Created by yitsol on 23-02-2018.
 */

public class AddProductPresenter implements IAddProductPresenter
{
    private AddProductActivity view;
    private ConnectionDetector connectionDetector;
    private IAddProductRepository repository;
    private IAddProductRepository.OnGetAllProductImgCallback productImgCallback;

    public AddProductPresenter(AddProductActivity view) {
        this.view = view;
        this.connectionDetector=new ConnectionDetector(view.getContext());
        repository=new AddProductRepository();
    }

    @Override
    public void getAllProductImgUrlDetails() {
        try {
            if(connectionDetector.isConnectedToInternet())
            {
                view.showLoaderNew();
                repository.getAllProductImgUrlDetails(view.getContext(),productImgCallback);
            }
            else
            {
                view.showMessage(AppConstant.CHECK_NETWORK_CONN);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void subscribeCallbacks() {
        productImgCallback=new IAddProductRepository.OnGetAllProductImgCallback() {
            @Override
            public void onSuccess(List<AppProductImgModel> productImgList) {
                view.hideloader();
                view.showProductImgListDialog(productImgList);
            }

            @Override
            public void onError(String message) {
                view.hideloader();
                view.showMessage(message);
            }
        };
    }

    @Override
    public void unSubscribeCallbacks() {
        productImgCallback=null;
    }
}
