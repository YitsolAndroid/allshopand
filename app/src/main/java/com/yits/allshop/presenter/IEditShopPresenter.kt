package com.yits.allshop.presenter

import com.yits.allshop.presenter.base.IBasePresenter

/**
 * Created by yitsol on 09-02-2018.
 */
interface IEditShopPresenter:IBasePresenter
{
    fun getShopDetailsById(shopId:String)
}