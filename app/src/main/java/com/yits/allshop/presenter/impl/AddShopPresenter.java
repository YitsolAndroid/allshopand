package com.yits.allshop.presenter.impl;

import android.support.design.widget.TextInputEditText;

import com.yits.allshop.model.AllShopDefImgModel;
import com.yits.allshop.model.AllShopType;
import com.yits.allshop.presenter.IAddShopPresenter;
import com.yits.allshop.repository.IAddShopRepository;
import com.yits.allshop.repository.impl.AddShopRepository;
import com.yits.allshop.utility.AppConstant;
import com.yits.allshop.utility.ConnectionDetector;
import com.yits.allshop.view.activity.AddShopActivity;
import com.yits.allshop.view.activity.EditShopActivity;

import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by yitsol on 07-02-2018.
 */

public class AddShopPresenter implements IAddShopPresenter {

    private AddShopActivity addShopView;
    private EditShopActivity editShopView;
    private ConnectionDetector connectionDetector;
    private IAddShopRepository addShopRepository;
    private IAddShopRepository.OnGetAllShopTypeCallback shopTypeCallback;
    private IAddShopRepository.OnGetAllShopDefImageCallback shopDefImageCallback;
    private IAddShopRepository.OnSaveNewShopCallback saveNewShopCallback;
    private static final String INDIANMOBILE_NUM_PATTERN = "^[789]\\d{9}$";
    private Matcher matcher;

    public AddShopPresenter(AddShopActivity view) {
        this.addShopView = view;
        this.connectionDetector = new ConnectionDetector(view.getActContext());
        addShopRepository = new AddShopRepository();
    }

    public AddShopPresenter(EditShopActivity view)
    {
        this.editShopView=view;
        this.connectionDetector = new ConnectionDetector(view.getActContext());
        addShopRepository = new AddShopRepository();
    }

    @Override
    public void subscribeCallbacks() {
        shopTypeCallback = new IAddShopRepository.OnGetAllShopTypeCallback() {
            @Override
            public void onSuccess(AllShopType allShopType) {
                if(addShopView!=null)
                {
                    addShopView.hideloader();
                    addShopView.showDiffShopType(allShopType);
                }
                else
                {
                    editShopView.hideloader();
                    editShopView.showDiffShopType(allShopType);
                }

            }

            @Override
            public void onError(String error) {
                if(addShopView!=null)
                {
                    addShopView.hideloader();
                    addShopView.showMessage(error);
                }
                else
                {
                    editShopView.hideloader();
                    editShopView.showMessage(error);
                }

            }
        };

        shopDefImageCallback = new IAddShopRepository.OnGetAllShopDefImageCallback() {
            @Override
            public void onSuccess(List<AllShopDefImgModel> defshopimglist) {
                if(addShopView!=null)
                {
                    addShopView.hideloader();
                    addShopView.showAllDefShopImg(defshopimglist);
                }
                else
                {
                    editShopView.hideloader();
                    editShopView.showAllDefShopImg(defshopimglist);
                }

            }

            @Override
            public void onError(String message) {
                addShopView.hideloader();
                addShopView.showMessage(message);
            }
        };

        saveNewShopCallback=new IAddShopRepository.OnSaveNewShopCallback() {
            @Override
            public void onSuccess() {
                addShopView.hideloader();
                addShopView.saveShopComplete();
            }

            @Override
            public void onError(String message) {
                addShopView.hideloader();
                addShopView.showMessage(message);
            }
        };
    }

    @Override
    public void unSubscribeCallbacks() {
        shopTypeCallback = null;
        shopDefImageCallback=null;
        saveNewShopCallback=null;
    }

    @Override
    public void getAllShopType() {
        try {
            if (connectionDetector.isConnectedToInternet()) {
                if(addShopView!=null)
                {
                    addShopView.showLoaderNew();
                    addShopRepository.getAllShopType(addShopView.getContext(), shopTypeCallback);
                }
                else
                {
                    editShopView.showLoaderNew();
                    addShopRepository.getAllShopType(editShopView.getContext(), shopTypeCallback);
                }

            } else {
                if(addShopView!=null)
                {
                    addShopView.showMessage(AppConstant.CHECK_NETWORK_CONN);
                }
                else
                {
                    editShopView.showMessage(AppConstant.CHECK_NETWORK_CONN);
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void getAllShopImage() {
        try {
            if(addShopView!=null)
            {
                addShopView.showLoaderNew();
                addShopRepository.getAllShopImage(addShopView.getContext(),shopDefImageCallback);
            }
            else
            {
                editShopView.showLoaderNew();
                addShopRepository.getAllShopImage(editShopView.getContext(),shopDefImageCallback);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void saveNewShop(@Nullable TextInputEditText edt_shopName,
                            @Nullable TextInputEditText edt_shopOwnerName,
                            @Nullable TextInputEditText edt_shopAddrs,
                            @Nullable TextInputEditText edt_shopPhone,
                            @Nullable String shopTypeId, @Nullable String shopTypeName,
                            @Nullable String shopImgUrl)
    {
        try
        {
            if(connectionDetector.isConnectedToInternet())
            {
                if(validateShopSave(edt_shopName,edt_shopOwnerName,edt_shopAddrs,edt_shopPhone,shopTypeId,
                        shopTypeName
                ,shopImgUrl))
                {
                    addShopView.showLoaderNew();
                    addShopRepository.saveNewShopDetails(getValueFromView(edt_shopName),
                            getValueFromView(edt_shopOwnerName),getValueFromView(edt_shopAddrs),
                            getValueFromView(edt_shopPhone),shopTypeId,shopTypeName,shopImgUrl,
                            saveNewShopCallback);
                }
            }
            else
            {
                addShopView.showMessage(AppConstant.CHECK_NETWORK_CONN);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private boolean validateShopSave(TextInputEditText edt_shopName, TextInputEditText edt_shopOwnerName,
                                     TextInputEditText edt_shopAddrs, TextInputEditText edt_shopPhone,
                                     String shopTypeId, String shopTypeName, String shopImgUrl)
    {
        if(getValueFromView(edt_shopName).equals("")&&getValueFromView(edt_shopName).length()==0
                &&getValueFromView(edt_shopName).isEmpty())
        {
            addShopView.showMessage("Please enter shop name");
            setErrorForView(edt_shopName,"Shop name is required");
            return false;
        }
        else if(getValueFromView(edt_shopOwnerName).equals("")&&getValueFromView(edt_shopOwnerName).length()==0
                &&getValueFromView(edt_shopOwnerName).isEmpty())
        {
            addShopView.showMessage("Please enter shop owner name");
            setErrorForView(edt_shopOwnerName,"Shop owner name is required");
            return false;
        }
        else if(getValueFromView(edt_shopAddrs).equals("")&&getValueFromView(edt_shopAddrs).length()==0&&
                getValueFromView(edt_shopAddrs).isEmpty())
        {
            addShopView.showMessage("Please enter shop address");
            setErrorForView(edt_shopAddrs,"Shop address is required");
            return false;
        }
        else if(getValueFromView(edt_shopPhone).equals("")&&getValueFromView(edt_shopPhone).length()==0&&
                getValueFromView(edt_shopPhone).isEmpty())
        {
            addShopView.showMessage("Please enter shop phone");
            setErrorForView(edt_shopPhone,"Shop phone is required");
            return false;
        }
        else if(!validate_mobnum_method(getValueFromView(edt_shopPhone)))
        {
            addShopView.showMessage("Mobile number invalid");
            setErrorForView(edt_shopPhone,"Shop phone is invalid");
            return false;
        }
        else if(shopTypeId.equals("NA"))
        {
            addShopView.showMessage("Select shop type");
            return false;
        }
        else if(shopImgUrl.equals("NA"))
        {
            addShopView.showMessage("Select shop default image");
            return false;
        }
        else
        {
            return true;
        }
    }

    private void setErrorForView(TextInputEditText edt_view, String msg)
    {
        edt_view.setError(msg);
    }

    private String getValueFromView(TextInputEditText edt_view)
    {
        return edt_view.getText().toString().trim();
    }

    private boolean validate_mobnum_method(String mobnum) {
        Pattern mobpattern = Pattern.compile(INDIANMOBILE_NUM_PATTERN);
        matcher = mobpattern.matcher(mobnum);
        return matcher.matches();
    }
}
