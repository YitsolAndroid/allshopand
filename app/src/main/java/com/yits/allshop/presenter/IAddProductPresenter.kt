package com.yits.allshop.presenter

import com.yits.allshop.presenter.base.IBasePresenter

/**
 * Created by yitsol on 23-02-2018.
 */
interface IAddProductPresenter:IBasePresenter
{
    fun getAllProductImgUrlDetails()
}