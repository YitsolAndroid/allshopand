package com.yits.allshop.presenter

import com.yits.allshop.model.ShopDataModel

/**
 * Created by yitsol on 08-02-2018.
 */
interface IShopListItemClickListener
{
    fun onItemClick(position: Int,datamodel: ShopDataModel)

    fun onItemEditClick(position: Int,datamodel: ShopDataModel)
}