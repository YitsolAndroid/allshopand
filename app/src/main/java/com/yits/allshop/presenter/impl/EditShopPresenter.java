package com.yits.allshop.presenter.impl;

import com.yits.allshop.model.ShopDetailsModel;
import com.yits.allshop.presenter.IEditShopPresenter;
import com.yits.allshop.repository.IEditShopRepository;
import com.yits.allshop.repository.impl.EditShopRepository;
import com.yits.allshop.utility.AppConstant;
import com.yits.allshop.utility.ConnectionDetector;
import com.yits.allshop.view.activity.EditShopActivity;

import org.jetbrains.annotations.NotNull;

/**
 * Created by yitsol on 09-02-2018.
 */

public class EditShopPresenter implements IEditShopPresenter
{
    private EditShopActivity view;
    private ConnectionDetector connectionDetector;
    private IEditShopRepository repository;
    private IEditShopRepository.OnGetShopDetailsByIdCallback detailsByIdCallback;

    public EditShopPresenter(EditShopActivity view) {
        this.view = view;
        connectionDetector=new ConnectionDetector(view.getContext());
        repository=new EditShopRepository();
    }

    @Override
    public void subscribeCallbacks() {
        detailsByIdCallback=new IEditShopRepository.OnGetShopDetailsByIdCallback() {
            @Override
            public void onSuccess(ShopDetailsModel detailsModel) {
                view.hideloader();
                view.shopdetailById(detailsModel);
            }

            @Override
            public void onError(String message) {
                view.hideloader();
            }
        };
    }

    @Override
    public void unSubscribeCallbacks() {
        detailsByIdCallback=null;
    }

    @Override
    public void getShopDetailsById(@NotNull String shopId) {
        try
        {
            if(connectionDetector.isConnectedToInternet())
            {
                view.showLoaderNew();
                repository.getShopDetailById(shopId,detailsByIdCallback);
            }
            else
            {
                view.showMessage(AppConstant.CHECK_NETWORK_CONN);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
