package com.yits.allshop.presenter.base

/**
 * Created by yitsol on 07-02-2018.
 */
interface IBasePresenter
{
    abstract fun subscribeCallbacks()
    abstract fun unSubscribeCallbacks()
}