package com.yits.allshop.presenter

import com.yits.allshop.model.ShopDataModel
import com.yits.allshop.presenter.base.IBasePresenter

/**
 * Created by yitsol on 08-02-2018.
 */
interface IShopListPresenter:IBasePresenter
{
    fun getAllShopDetails()
    fun deleteShopFromPosition(datamodel: ShopDataModel)
}