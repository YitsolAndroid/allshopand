package com.yits.allshop.presenter;

import android.support.design.widget.TextInputEditText;

import com.yits.allshop.presenter.base.IBasePresenter;

import org.jetbrains.annotations.Nullable;

/**
 * Created by yitsol on 07-02-2018.
 */

public interface IAddShopPresenter extends IBasePresenter {
    void getAllShopType();

    void getAllShopImage();

    void saveNewShop(@Nullable TextInputEditText edt_shopName, @Nullable TextInputEditText edt_shopOwnerName,
                     @Nullable TextInputEditText edt_shopAddrs, @Nullable TextInputEditText edt_shopPhone,
                     @Nullable String shopTypeId, @Nullable String shopTypeName, @Nullable String shopImgUrl);
}
