package com.yits.allshop.utility;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Goutam on 10/3/2015.
 * Class used to set the desired font to the app screen.
 */
public class FontType
{
    public Typeface trajanprotypeFace_element,opensanstypeFace_element,swisswztypeface_element;

    private final String ELEMENT_FONT= "segoeui.ttf";

    private Context context;


    public FontType(Context con)
    {
        this.context=con;
       // trajanprotypeFace_element=Typeface.createFromAsset(con.getAssets(), "fonts/segoeui.ttf");
        trajanprotypeFace_element=Typeface.createFromAsset(con.getAssets(), "fonts/trajanpro_regular.ttf");
        opensanstypeFace_element=Typeface.createFromAsset(con.getAssets(), "fonts/OpenSans-Regular.ttf");
    }

    public FontType(Context con, ViewGroup root)
    {
        //trajanprotypeFace_element=Typeface.createFromAsset(con.getAssets(), "fonts/segoeui.ttf");
        trajanprotypeFace_element=Typeface.createFromAsset(con.getAssets(), "fonts/trajanpro_regular.ttf");
        setFont(root, trajanprotypeFace_element);
    }

    public void SetOpenSansFontType(Context con, ViewGroup root)
    {
        opensanstypeFace_element=Typeface.createFromAsset(con.getAssets(), "fonts/OpenSans-Regular.ttf");
        setFont(root, opensanstypeFace_element);
    }

    public FontType(Context context, ViewGroup root, String classname)
    {

    }

    public void setapptitlefont(TextView tv_firstapptitle,TextView tv_secapptitle,TextView tv_thirdapptitle)
    {
        tv_firstapptitle.setTypeface(trajanprotypeFace_element);
        tv_secapptitle.setTypeface(trajanprotypeFace_element);
        tv_thirdapptitle.setTypeface(trajanprotypeFace_element);
    }

    public void settextViewFont(TextView textView, Typeface font) {

        textView.setTypeface(font);
    }


    public void setFont(ViewGroup group, Typeface font) {
        int count = group.getChildCount();
        View v;
        for (int i = 0; i < count; i++) {
            v = group.getChildAt(i);
            if (v instanceof TextView) {
                ((TextView) v).setTypeface(font);
            }
            else if (v instanceof ViewGroup)
                setFont((ViewGroup) v, font);
        }
    }
}
