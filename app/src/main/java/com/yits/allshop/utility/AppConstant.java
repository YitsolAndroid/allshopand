package com.yits.allshop.utility;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by yitsol on 06-02-2018.
 */

public interface AppConstant
{
     String BAKERYSHOPURL="https://s3-ap-southeast-1.amazonaws.com/telepathytransport/brakery_shop.jpg";
     String CHICKENSHOPURL="https://s3-ap-southeast-1.amazonaws.com/telepathytransport/chicken_shop.jpg";
     String FRUITVEGSHOPURL="https://s3-ap-southeast-1.amazonaws.com/telepathytransport/fruitveg_shop.jpg";
     String GARMENTSHOPURL="https://s3-ap-southeast-1.amazonaws.com/telepathytransport/garment_shop.png";
     String GROCERYSHOPURL="https://s3-ap-southeast-1.amazonaws.com/telepathytransport/grocery_shop.jpg";
     String JWELLERYSHOPURL="https://s3-ap-southeast-1.amazonaws.com/telepathytransport/jewellery_shop.jpg";
     String MEDICALSHOPURL="https://s3-ap-southeast-1.amazonaws.com/telepathytransport/medical_shop.jpg";

     String ADDNEWSHOP="Add New Shop";
     String ALLSHOPLIST="Shop List";
     String SUCCESS_RESPONSE = "success";
     String CHECK_NETWORK_CONN = "Please check internet connection";
     String GETERROR = "Server GET error";
     String DEFIMGGETERROR = "Error while getting shop image";
    @Nullable
    String SHOPID="shopId";


    @Nullable
    String DASHBOARD="Dashboard";
    @NotNull
    String ADDNEWPRODUCT="Add New Product";

    String FRUITVEGPRODUCT_IMG="https://s3-ap-southeast-1.amazonaws.com/telepathytransport/fruitveg_product.jpg";
    String CHICKENPRODUCT_IMG="https://s3-ap-southeast-1.amazonaws.com/telepathytransport/chicken_product.png";
    String GARMENTPRODUCT_IMG="https://s3-ap-southeast-1.amazonaws.com/telepathytransport/garment_product.jpg";
    String GROCERYPRODUCT_IMG="https://s3-ap-southeast-1.amazonaws.com/telepathytransport/grocery_product.png";
    String JWELLERYPRODUCT_IMG="https://s3-ap-southeast-1.amazonaws.com/telepathytransport/jwellery_product.jpg";
    String MEDICALPRODUCT_IMG="https://s3-ap-southeast-1.amazonaws.com/telepathytransport/medical_product.jpg";
    String BAKERYPRODUCT_IMG="https://s3-ap-southeast-1.amazonaws.com/telepathytransport/brakery_product.png";

}
