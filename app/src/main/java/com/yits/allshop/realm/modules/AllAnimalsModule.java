package com.yits.allshop.realm.modules;

import io.realm.annotations.RealmModule;

/**
 * Created by yitsol on 01-03-2018.
 */
@RealmModule(allClasses = true)
public class AllAnimalsModule {
}
