package com.yits.allshop.realm.modules;

import com.yits.allshop.realm.model.Cat;
import com.yits.allshop.realm.model.Dog;

import io.realm.annotations.RealmModule;

/**
 * Created by yitsol on 01-03-2018.
 */
@RealmModule(classes = {Cat.class, Dog.class})
public class DomesticAnimalsModule {
}
