package com.yits.allshop.realm.model;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by yitsol on 01-03-2018.
 */

public class Pig extends RealmObject
{
    private String name;
    // It is possible for model classes to to reference library model classes as long
    // as they all are included in the schema when opening the Realm.
    private RealmList<Dog> afraidOf = new RealmList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
