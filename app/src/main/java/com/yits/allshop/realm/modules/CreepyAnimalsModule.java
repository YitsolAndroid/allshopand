package com.yits.allshop.realm.modules;


import com.yits.allshop.realm.model.Snake;
import com.yits.allshop.realm.model.Spider;

import io.realm.annotations.RealmModule;

/**
 * Created by yitsol on 01-03-2018.
 */
@RealmModule(classes = {Snake.class, Spider.class})
public class CreepyAnimalsModule {
}
