package com.yits.allshop.realm.modules;

import com.yits.allshop.realm.model.Elephant;
import com.yits.allshop.realm.model.Lion;
import com.yits.allshop.realm.model.Zebra;

import io.realm.annotations.RealmModule;

/**
 * Created by yitsol on 01-03-2018.
 */
@RealmModule(classes = {Elephant.class, Lion.class, Zebra.class})
public class ZooAnimalsModule {
}
