package com.yits.allshop.realm.model;

import io.realm.RealmObject;

/**
 * Created by yitsol on 01-03-2018.
 */

public class Snake extends RealmObject
{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
