package com.yits.allshop.realm.dbconfig;


import android.support.annotation.NonNull;

import com.yits.allshop.realm.model.Cat;
import com.yits.allshop.realm.modules.AllAnimalsModule;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by yitsol on 01-03-2018.
 */

public class Zoo
{
    private final RealmConfiguration realmConfig;
    private Realm realm;

    public Zoo() {
        realmConfig = new RealmConfiguration.Builder()     // The app is responsible for calling `Realm.init(Context)`
                .name("library.zoo.realm")                 // So always use a unique name
                .modules(new AllAnimalsModule())           // Always use explicit modules in library projects
                .build();

        // Reset Realm
        Realm.deleteRealm(realmConfig);
    }

    public void open() {
        // Don't use Realm.setDefaultInstance() in library projects. It is unsafe as app developers can override the
        // default configuration. So always use explicit configurations in library projects.
        realm = Realm.getInstance(realmConfig);
    }

    public long getNoOfAnimals() {
        return realm.where(Cat.class).count();
    }

    public void addAnimals(final int count) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                for (int i = 0; i < count; i++) {
                    Cat cat = realm.createObject(Cat.class);
                    cat.setName("Cat " + i);
                }
            }
        });
    }

    public void close() {
        realm.close();
    }
}
