package com.yits.allshop.repository.impl;

import com.yits.allshop.model.ShopDetailsModel;
import com.yits.allshop.repository.IEditShopRepository;
import com.yits.allshop.service.APIService;
import com.yits.allshop.service.ApiUtils;

import org.json.JSONObject;

import java.net.HttpURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yitsol on 09-02-2018.
 */

public class EditShopRepository implements IEditShopRepository
{
    private APIService apiService;

    public EditShopRepository() {
        apiService= ApiUtils.getAPIService();
    }

    @Override
    public void getShopDetailById(String shopId, final OnGetShopDetailsByIdCallback callback) {
        try
        {
            JSONObject filterjobj=new JSONObject();
            JSONObject wherejobj=new JSONObject();
            filterjobj.put("id",shopId);
            wherejobj.put("where",filterjobj);
            apiService.getShopDetailsByID(wherejobj.toString()).enqueue(new Callback<ShopDetailsModel>() {
                @Override
                public void onResponse(Call<ShopDetailsModel> call, Response<ShopDetailsModel> response) {
                    if(callback!=null)
                    {
                        if(response.code()== HttpURLConnection.HTTP_OK)
                        {
                            ShopDetailsModel detailsModel=response.body();
                            callback.onSuccess(detailsModel);
                        }
                        else
                        {
                            callback.onError("error while getting shop detail by id");
                        }
                    }
                }

                @Override
                public void onFailure(Call<ShopDetailsModel> call, Throwable t) {

                }
            });
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
