package com.yits.allshop.repository;

import android.content.Context;

import com.yits.allshop.model.AllShopDefImgModel;
import com.yits.allshop.model.AllShopType;
import com.yits.allshop.repository.base.IBaseRepository;

import java.util.List;

/**
 * Created by yitsol on 07-02-2018.
 */

public interface IAddShopRepository extends IBaseRepository {

    void getAllShopType(Context actContext, OnGetAllShopTypeCallback callback);

    void getAllShopImage(Context actContext,OnGetAllShopDefImageCallback callback);

    void saveNewShopDetails(String shopName, String shopOwnerName, String shopAddrs,
                            String shopPhone, String shopTypeId, String shopTypeName, String shopImgUrl
    ,OnSaveNewShopCallback callback);

    interface OnSaveNewShopCallback
    {
        void onSuccess();
        void onError(String message);
    }

    interface OnGetAllShopDefImageCallback
    {
        void onSuccess(List<AllShopDefImgModel> defshopimglist);
        void onError(String message);
    }

    interface OnGetAllShopTypeCallback
    {
        void onSuccess(AllShopType allShopType);
        void onError(String error);
    }
}
