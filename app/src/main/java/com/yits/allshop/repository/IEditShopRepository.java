package com.yits.allshop.repository;

import com.yits.allshop.model.ShopDetailsModel;
import com.yits.allshop.repository.base.IBaseRepository;

/**
 * Created by yitsol on 09-02-2018.
 */

public interface IEditShopRepository extends IBaseRepository
{


    void getShopDetailById(String shopId ,OnGetShopDetailsByIdCallback callback);
    interface OnGetShopDetailsByIdCallback
    {
        void onSuccess(ShopDetailsModel detailsModel);
        void onError(String message);
    }
}
