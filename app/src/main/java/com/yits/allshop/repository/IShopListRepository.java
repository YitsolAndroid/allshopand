package com.yits.allshop.repository;

import android.content.Context;

import com.yits.allshop.model.ShopDataModel;
import com.yits.allshop.repository.base.IBaseRepository;

import java.util.List;

/**
 * Created by yitsol on 08-02-2018.
 */

public interface IShopListRepository extends IBaseRepository
{

    void getAllShopDetails(Context context,OnGetAllShopDetailsCallback callback);

    void deleteShopData(ShopDataModel datamodel,OnDeleteShopCallback callback);

    interface OnDeleteShopCallback
    {
        void onSuccess();
        void onError(String message);
    }

    interface OnGetAllShopDetailsCallback
    {
        void onSuccess(List<ShopDataModel> detailsModel);
        void onError(String message);

    }
}
