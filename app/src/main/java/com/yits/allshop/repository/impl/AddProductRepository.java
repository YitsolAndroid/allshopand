package com.yits.allshop.repository.impl;

import android.content.Context;

import com.yits.allshop.model.AppProductImgModel;
import com.yits.allshop.repository.IAddProductRepository;
import com.yits.allshop.utility.AppConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yitsol on 23-02-2018.
 */

public class AddProductRepository implements IAddProductRepository
{

    @Override
    public void getAllProductImgUrlDetails(Context context, OnGetAllProductImgCallback callback) {
        try {
            List<AppProductImgModel> productImgList=new ArrayList<>();
            productImgList.add(new AppProductImgModel("Bakery", AppConstant.BAKERYPRODUCT_IMG));
            productImgList.add(new AppProductImgModel("Chicken",AppConstant.CHICKENPRODUCT_IMG));
            productImgList.add(new AppProductImgModel("Garment",AppConstant.GARMENTPRODUCT_IMG));
            productImgList.add(new AppProductImgModel("Grocery",AppConstant.GROCERYPRODUCT_IMG));
            productImgList.add(new AppProductImgModel("Jewellery",AppConstant.JWELLERYPRODUCT_IMG));
            productImgList.add(new AppProductImgModel("Medical",AppConstant.MEDICALPRODUCT_IMG));
            productImgList.add(new AppProductImgModel("FruitVeg",AppConstant.FRUITVEGPRODUCT_IMG));
            if(callback!=null)
            {
                callback.onSuccess(productImgList);
            }
        }
        catch (Exception ex)
        {
            if(callback!=null)
            {
                callback.onError("errror while getting product image url");
            }
            ex.printStackTrace();
        }
    }
}
