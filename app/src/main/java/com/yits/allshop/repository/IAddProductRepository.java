package com.yits.allshop.repository;

import android.content.Context;

import com.yits.allshop.model.AppProductImgModel;
import com.yits.allshop.repository.base.IBaseRepository;

import java.util.List;

/**
 * Created by yitsol on 23-02-2018.
 */

public interface IAddProductRepository extends IBaseRepository
{
    interface OnGetAllProductImgCallback
    {
        void onSuccess(List<AppProductImgModel> productImgList);
        void onError(String message);
    }

    void getAllProductImgUrlDetails(Context context,OnGetAllProductImgCallback callback);
}
