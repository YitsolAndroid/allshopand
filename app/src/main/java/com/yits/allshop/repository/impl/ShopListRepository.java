package com.yits.allshop.repository.impl;

import android.content.Context;

import com.yits.allshop.model.ShopDataModel;
import com.yits.allshop.model.ShopDeleteResultModel;
import com.yits.allshop.model.ShopDetailsModel;
import com.yits.allshop.repository.IShopListRepository;
import com.yits.allshop.service.APIService;
import com.yits.allshop.service.ApiUtils;

import java.net.HttpURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yitsol on 08-02-2018.
 */

public class ShopListRepository implements IShopListRepository
{
    private APIService apiService;

    public ShopListRepository() {
        apiService= ApiUtils.getAPIService();
    }

    @Override
    public void getAllShopDetails(Context context, final OnGetAllShopDetailsCallback callback) {
        try {
            apiService.getAllShopDetails().enqueue(new Callback<ShopDetailsModel>() {
                @Override
                public void onResponse(Call<ShopDetailsModel> call, Response<ShopDetailsModel> response) {
                    if(callback!=null)
                    {
                        if(response.code()== HttpURLConnection.HTTP_OK)
                        {
                            ShopDetailsModel detailsModel=response.body();
                            if(detailsModel.getData()!=null&&detailsModel.getData().size()>0)
                            {
                                callback.onSuccess(detailsModel.getData());
                            }
                            else
                            {
                                callback.onError("shop detail not found");
                            }

                        }
                        else
                        {
                            callback.onError("error while getting shop details");
                        }
                    }
                }

                @Override
                public void onFailure(Call<ShopDetailsModel> call, Throwable t) {

                }
            });
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void deleteShopData(ShopDataModel datamodel, final OnDeleteShopCallback callback) {
        try {
            apiService.deleteShopById(datamodel.getId()).enqueue(new Callback<ShopDeleteResultModel>() {
                @Override
                public void onResponse(Call<ShopDeleteResultModel> call, Response<ShopDeleteResultModel> response) {
                    if(callback!=null)
                    {
                        if(response.code()==HttpURLConnection.HTTP_OK)
                        {
                            ShopDeleteResultModel resultModel=response.body();
                            if(resultModel.getData()!=null&&resultModel.getData().getCount()==1)
                            {
                                callback.onSuccess();
                            }
                            else
                            {
                                callback.onError("error while deleting shop");
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<ShopDeleteResultModel> call, Throwable t) {

                }
            });
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
