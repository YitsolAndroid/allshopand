package com.yits.allshop.repository.impl;

import android.content.Context;

import com.yits.allshop.model.AllShopDefImgModel;
import com.yits.allshop.model.AllShopType;
import com.yits.allshop.model.NewShopInputModel;
import com.yits.allshop.model.NewShopOutputModel;
import com.yits.allshop.repository.IAddShopRepository;
import com.yits.allshop.service.APIService;
import com.yits.allshop.service.ApiUtils;
import com.yits.allshop.utility.AppConstant;
import com.yits.allshop.utility.LoggerUtils;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yitsol on 07-02-2018.
 */

public class AddShopRepository implements IAddShopRepository
{
    private APIService apiService;
    public AddShopRepository() {
        apiService= ApiUtils.getAPIService();
    }


    @Override
    public void getAllShopType(Context actContext, final OnGetAllShopTypeCallback callback) {
        try {
            apiService.getAllDiffShopType().enqueue(new Callback<AllShopType>() {
                @Override
                public void onResponse(Call<AllShopType> call, Response<AllShopType> response) {
                    if(callback!=null) {
                        LoggerUtils.error(AddShopRepository.class.getSimpleName(),"shop type response "+response.code());
                        if(response.code()== HttpURLConnection.HTTP_OK)
                        {
                            AllShopType allShopType=response.body();
                            callback.onSuccess(allShopType);
                        }
                        else
                        {
                            callback.onError(AppConstant.GETERROR);
                        }
                    }
                }

                @Override
                public void onFailure(Call<AllShopType> call, Throwable t) {

                }
            });
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void getAllShopImage(Context actContext, OnGetAllShopDefImageCallback callback) {
        try {
            List<AllShopDefImgModel> defshopimglist=new ArrayList<>();
            defshopimglist.add(new AllShopDefImgModel(1,AppConstant.BAKERYSHOPURL,false));
            defshopimglist.add(new AllShopDefImgModel(2,AppConstant.CHICKENSHOPURL,false));
            defshopimglist.add(new AllShopDefImgModel(3,AppConstant.GARMENTSHOPURL,false));
            defshopimglist.add(new AllShopDefImgModel(4,AppConstant.GROCERYSHOPURL,false));
            defshopimglist.add(new AllShopDefImgModel(5,AppConstant.JWELLERYSHOPURL,false));
            defshopimglist.add(new AllShopDefImgModel(6,AppConstant.MEDICALSHOPURL,false));
            defshopimglist.add(new AllShopDefImgModel(7,AppConstant.FRUITVEGSHOPURL,false));
            if(callback!=null)
            {
                callback.onSuccess(defshopimglist);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            if(callback!=null)
            {
                callback.onError(AppConstant.DEFIMGGETERROR);
            }
        }
    }

    @Override
    public void saveNewShopDetails(String shopName, String shopOwnerName, String shopAddrs,
                                   String shopPhone, String shopTypeId, String shopTypeName,
                                   String shopImgUrl, final OnSaveNewShopCallback callback) {
        try
        {
            NewShopInputModel inputModel=new NewShopInputModel(shopName
            ,"1111",shopOwnerName,shopAddrs,shopPhone,shopImgUrl,shopTypeId
            ,shopTypeName);
            apiService.saveNewShop(inputModel).enqueue(new Callback<NewShopOutputModel>() {
                @Override
                public void onResponse(Call<NewShopOutputModel> call, Response<NewShopOutputModel> response) {
                    if(callback!=null)
                    {
                        if(response.code()==HttpURLConnection.HTTP_OK)
                        {
                            NewShopOutputModel outputModel=response.body();
                            callback.onSuccess();
                        }
                        else
                        {
                            callback.onError("error while saving shop");
                        }
                    }
                }

                @Override
                public void onFailure(Call<NewShopOutputModel> call, Throwable t) {

                }
            });
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
