package com.yits.allshop.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by yitsol on 08-02-2018.
 */
data class ShopDeleteResultModel(@SerializedName("status")
                                 @Expose
                                 var status: String? = null,
                                 @SerializedName("message")
                                 @Expose
                                 var message: String? = null,
                                 @SerializedName("data")
                                 @Expose
                                 var data: DeleteDataModel? = null)