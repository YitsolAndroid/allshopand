package com.yits.allshop.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by yitsol on 08-02-2018.
 */
data class ShopDataModel(@SerializedName("shopName")
                         @Expose
                         var shopName: String? = null,
                         @SerializedName("shopLicNum")
                         @Expose
                         var shopLicNum: String? = null,
                         @SerializedName("shopOwnerName")
                         @Expose
                         var shopOwnerName: String? = null,
                         @SerializedName("shopAddress")
                         @Expose
                         var shopAddress: String? = null,
                         @SerializedName("shopPhone")
                         @Expose
                         var shopPhone: String? = null,
                         @SerializedName("shopImage")
                         @Expose
                         var shopImage: String? = null,
                         @SerializedName("shopTypeId")
                         @Expose
                         var shopTypeId: String? = null,
                         @SerializedName("shopType")
                         @Expose
                         var shopType: String? = null,
                         @SerializedName("id")
                         @Expose
                         var id: String? = null,
                         @SerializedName("createdDate")
                         @Expose
                         var createdDate: String? = null)