package com.yits.allshop.model

/**
 * Created by yitsol on 23-02-2018.
 */
data class AppProductImgModel(var prodImgType:String,var prodImgUrl:String) {
}