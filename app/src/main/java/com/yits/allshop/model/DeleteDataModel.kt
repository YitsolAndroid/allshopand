package com.yits.allshop.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



/**
 * Created by yitsol on 08-02-2018.
 */
data class DeleteDataModel(@SerializedName("count")
                               @Expose
                               var count: Long = 0)