package com.yits.allshop.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yitsol on 08-02-2018.
 */

public class NewShopInputModel
{

    @SerializedName("shopName")
    @Expose
    private String shopName;
    @SerializedName("shopLicNum")
    @Expose
    private String shopLicNum;
    @SerializedName("shopOwnerName")
    @Expose
    private String shopOwnerName;
    @SerializedName("shopAddress")
    @Expose
    private String shopAddress;
    @SerializedName("shopPhone")
    @Expose
    private String shopPhone;
    @SerializedName("shopImage")
    @Expose
    private String shopImage;
    @SerializedName("shopTypeId")
    @Expose
    private String shopTypeId;
    @SerializedName("shopType")
    @Expose
    private String shopType;

    public NewShopInputModel(String shopName, String shopLicNum, String shopOwnerName, String shopAddress, String shopPhone, String shopImage, String shopTypeId, String shopType) {
        this.shopName = shopName;
        this.shopLicNum = shopLicNum;
        this.shopOwnerName = shopOwnerName;
        this.shopAddress = shopAddress;
        this.shopPhone = shopPhone;
        this.shopImage = shopImage;
        this.shopTypeId = shopTypeId;
        this.shopType = shopType;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopLicNum() {
        return shopLicNum;
    }

    public void setShopLicNum(String shopLicNum) {
        this.shopLicNum = shopLicNum;
    }

    public String getShopOwnerName() {
        return shopOwnerName;
    }

    public void setShopOwnerName(String shopOwnerName) {
        this.shopOwnerName = shopOwnerName;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getShopPhone() {
        return shopPhone;
    }

    public void setShopPhone(String shopPhone) {
        this.shopPhone = shopPhone;
    }

    public String getShopImage() {
        return shopImage;
    }

    public void setShopImage(String shopImage) {
        this.shopImage = shopImage;
    }

    public String getShopTypeId() {
        return shopTypeId;
    }

    public void setShopTypeId(String shopTypeId) {
        this.shopTypeId = shopTypeId;
    }

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }
}
