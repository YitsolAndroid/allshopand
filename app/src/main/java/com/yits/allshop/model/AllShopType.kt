package com.yits.allshop.model
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
/**
 * Created by yitsol on 07-02-2018.
 */
data class AllShopType(@SerializedName("status") @Expose var status:String,@SerializedName("message") @Expose var message:String,@SerializedName("data")@Expose var data:List<ShopTypeDataModel>)