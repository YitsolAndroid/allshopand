package com.yits.allshop.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by yitsol on 07-02-2018.
 */
class ShopTypeDataModel(@SerializedName("typeId")
                        @Expose
                        var typeId: String,
                        @SerializedName("typeName")
                        @Expose
                        var typeName: String
                        , @SerializedName("id")
                        @Expose
                        var id: String,var selectStatus:Boolean)
