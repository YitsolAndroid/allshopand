package com.yits.allshop.application;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by yitsol on 01-03-2018.
 */

public class ShopApplication extends Application
{
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);

    }
}
