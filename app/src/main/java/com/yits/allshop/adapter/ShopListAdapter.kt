package com.yits.allshop.adapter

import android.content.Context
import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.yits.allshop.R
import com.yits.allshop.model.ShopDataModel
import com.yits.allshop.presenter.IShopListItemClickListener
import kotlinx.android.synthetic.main.row_shopdetails.view.*

/**
 * Created by yitsol on 08-02-2018.
 */
class ShopListAdapter(context: Context,datalist:List<ShopDataModel>):RecyclerView.Adapter<ShopListAdapter.ViewHolder>()
{
    private var _context:Context?=null
    private var shopList:List<ShopDataModel>?=null
    private var listener:IShopListItemClickListener?=null

    init {
        this._context=context
        this.shopList=datalist
    }

    fun registerListener(clickListener: IShopListItemClickListener)
    {
        this.listener=clickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val shopView=LayoutInflater.from(_context).inflate(R.layout.row_shopdetails,parent,false)
        return ViewHolder(shopView)
    }

    override fun getItemCount(): Int {
        return this.shopList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bindItems(shopList!!.get(position),position)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(datamodel: ShopDataModel, position: Int) {
            showShopImage(itemView.img_shopImage,datamodel)
            itemView.txt_shopName.setText(datamodel.shopName)
            itemView.txt_shopownername.setText(datamodel.shopOwnerName)
            itemView.txt_shopphone.setText(datamodel.shopPhone)
            itemView.txt_shopAddrs.setText(datamodel.shopAddress)
            itemView.txt_shopType.setText(datamodel.shopType)
            itemView.img_deleteShop.setOnClickListener(object:View.OnClickListener{
                override fun onClick(v: View?) {
                    listener?.onItemClick(position,datamodel)
                }
            })
            itemView.img_editShop.setOnClickListener(object :View.OnClickListener
            {
                override fun onClick(v: View?) {
                    listener?.onItemEditClick(position,datamodel)
                }
            })
        }
    }

    private fun showShopImage(img_shopImage: ImageView?, datamodel: ShopDataModel) {
        val reqoption: RequestOptions = RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.mipmap.ic_launcher)
                .fitCenter()
                .error(R.mipmap.ic_launcher)
        Glide.with(_context)
                .asBitmap()
                .load(datamodel.shopImage)
                .apply(reqoption)
                .into(object : SimpleTarget<Bitmap>(){
                    override fun onResourceReady(resource: Bitmap?, transition: Transition<in Bitmap>?) {
                        img_shopImage?.setImageBitmap(resource)
                    }
                })
    }

}