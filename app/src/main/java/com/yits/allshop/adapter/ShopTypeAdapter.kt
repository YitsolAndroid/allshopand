package com.yits.allshop.adapter

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.yits.allshop.R
import com.yits.allshop.model.ShopTypeDataModel
import com.yits.allshop.presenter.IShopTypeItemListener
import kotlinx.android.synthetic.main.row_diffshoptype.view.*

/**
 * Created by yitsol on 07-02-2018.
 */
class ShopTypeAdapter(context: Context, datalist: List<ShopTypeDataModel>) : RecyclerView.Adapter<ShopTypeAdapter.ViewHolder>() {
    private var datalist: List<ShopTypeDataModel>? = null
    private var context: Context? = null
    private var listener: IShopTypeItemListener? = null

    init {
        this.context = context;
        this.datalist = datalist;
    }

    fun registerItemClickListener(clickListener: IShopTypeItemListener) {
        this.listener = clickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val typeview = LayoutInflater.from(context).inflate(R.layout.row_diffshoptype, parent, false)
        return ViewHolder(typeview)
    }

    override fun getItemCount(): Int {
        return datalist!!.count()
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bindItems(datalist?.get(position), context)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(datamodel: ShopTypeDataModel?, context: Context?) {
            if(datamodel!!.selectStatus)
            {
                itemView.txt_shoptype.setBackgroundColor(ContextCompat.getColor(context!!,R.color.green))
            }
            else
            {
                itemView.txt_shoptype.
                        setBackgroundColor(ContextCompat.getColor(context!!,R.color.md_indigo_500))
            }
            itemView.txt_shoptype.setText(datamodel.typeName)
            itemView.txt_shoptype.setOnClickListener(View.OnClickListener {
                changeSelectionColor(datamodel)
                listener?.onShopTypeClick(datamodel.id, datamodel.typeId, datamodel.typeName)
                notifyDataSetChanged()
            })
        }
    }

    private fun changeSelectionColor(datamodel: ShopTypeDataModel) {
        for (model in this.datalist!!)
        {
            model.selectStatus=false
        }
        datamodel.selectStatus=true
    }
}