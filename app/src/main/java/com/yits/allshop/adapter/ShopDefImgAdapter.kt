package com.yits.allshop.adapter

import android.content.Context
import android.graphics.Bitmap
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.yits.allshop.R
import com.yits.allshop.model.AllShopDefImgModel
import com.yits.allshop.presenter.IDefImgClickListener
import kotlinx.android.synthetic.main.row_shopdefimage.view.*

/**
 * Created by yitsol on 07-02-2018.
 */
class ShopDefImgAdapter(context: Context, defshopimglist: List<AllShopDefImgModel>) : RecyclerView.Adapter<ShopDefImgAdapter.ViewHolder>() {
    var context: Context? = null
    private var shopImgList: List<AllShopDefImgModel>? = null
    private var listener: IDefImgClickListener? = null

    init {
        this.context = context
        this.shopImgList = defshopimglist
    }

    fun registerClickListener(clicklistener: IDefImgClickListener) {
        this.listener = clicklistener
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val shopImgView = LayoutInflater.from(context).inflate(R.layout.row_shopdefimage, parent, false)
        return ViewHolder(shopImgView)
    }

    override fun getItemCount(): Int {
        return this.shopImgList!!.count()
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bindItems(this.shopImgList?.get(position),context)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(imgmodel: AllShopDefImgModel?, context: Context?) {
            if (imgmodel!!.selectStatus) {
                itemView.card_view_shopImg.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.green))
            } else {
                itemView.card_view_shopImg.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.md_indigo_500))
            }
            showShopImage(itemView.img_shopdef, imgmodel)
            itemView.img_shopdef.setOnClickListener(View.OnClickListener {
                changeSelectionColor(imgmodel)
                listener?.OnDefImgClickListner(imgmodel.index, imgmodel.imgurl)
                notifyDataSetChanged()
            })
        }

    }

    private fun changeSelectionColor(imgmodel: AllShopDefImgModel) {
        for (model in this.shopImgList!!) {
            model.selectStatus = false
        }
        imgmodel.selectStatus = true
    }

    private fun showShopImage(img_shopdef: ImageView?, imgmodel: AllShopDefImgModel?) {
        val reqoption: RequestOptions = RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.mipmap.ic_launcher)
                .fitCenter()
                .error(R.mipmap.ic_launcher)
        Glide.with(context)
                .asBitmap()
                .load(imgmodel?.imgurl)
                .apply(reqoption)
                .into(object : SimpleTarget<Bitmap>() {
                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>) {
                        img_shopdef?.setImageBitmap(resource)
                    }
                })
    }
}