package com.yits.allshop.service;

import com.yits.allshop.model.AllShopType;
import com.yits.allshop.model.NewShopInputModel;
import com.yits.allshop.model.NewShopOutputModel;
import com.yits.allshop.model.ShopDeleteResultModel;
import com.yits.allshop.model.ShopDetailsModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by yitsol on 07-02-2018.
 */

public interface APIService
{
    @GET("shopTypes")
    Call<AllShopType> getAllDiffShopType();

    @POST("shops")
    Call<NewShopOutputModel> saveNewShop(@Body NewShopInputModel inputModel);

    @GET("shops")
    Call<ShopDetailsModel> getAllShopDetails();

    @DELETE("shops/{id}")
    Call<ShopDeleteResultModel> deleteShopById(@Path("id") String shopId);

    @GET("shops")
    Call<ShopDetailsModel> getShopDetailsByID(@Query("filter") String filter);
}
