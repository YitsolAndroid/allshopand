package com.yits.allshop.service;

/**
 * Created by yitsol on 07-02-2018.
 */

public class ApiUtils
{
    private ApiUtils() {}

    public static final String BASE_URL="http://192.168.0.54:3000/api/";
    public static APIService getAPIService() {
        return RetrofitClient.
                getClient(BASE_URL).
                create(APIService.class);
    }
}
