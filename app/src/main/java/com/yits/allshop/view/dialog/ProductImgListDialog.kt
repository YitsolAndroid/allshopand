package com.yits.allshop.view.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatDialog
import android.view.Window
import com.yits.allshop.R

/**
 * Created by yitsol on 23-02-2018.
 */
class ProductImgListDialog(context: Context?) : AppCompatDialog(context)
{
    internal var _context: Context?=null

    init {
        this._context=context;
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window!!.attributes.windowAnimations = R.style.OSFilterDialogTheme
        this.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setContentView(R.layout.appproductimglistdialog)

    }
}