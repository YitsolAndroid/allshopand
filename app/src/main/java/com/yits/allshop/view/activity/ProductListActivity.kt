package com.yits.allshop.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.yits.allshop.R
import com.yits.allshop.utility.AppConstant
import com.yits.allshop.view.activity.AddProductActivity
import com.yits.allshop.view.activity.Dashboard
import com.yits.allshop.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_product_list.*


class ProductListActivity : BaseActivity() {
    override fun getContext(): Context {
        return this@ProductListActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list)
        setSupportActionBar(toolbar)
        supportActionBar?.title = AppConstant.ADDNEWSHOP
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        txt_addnewproduct.setOnClickListener({
            gotoAddProductScreen()
        })
    }

    private fun gotoAddProductScreen() {
        val addprodIntent=Intent(getContext(), AddProductActivity::class.java)
        startActivity(addprodIntent)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId)
        {
            android.R.id.home->gobacktoScreen()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun gobacktoScreen() {
        val homeIntent= Intent(getContext(), Dashboard::class.java)
        startActivity(homeIntent)
    }

}
