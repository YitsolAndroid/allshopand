package com.yits.allshop.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.yits.allshop.R
import com.yits.allshop.realm.dbconfig.Zoo
import com.yits.allshop.realm.model.*
import com.yits.allshop.realm.modules.CreepyAnimalsModule
import com.yits.allshop.realm.modules.DomesticAnimalsModule
import com.yits.allshop.realm.modules.ZooAnimalsModule
import com.yits.allshop.utility.AppConstant
import com.yits.allshop.view.base.BaseActivity
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.exceptions.RealmException
import kotlinx.android.synthetic.main.activity_dashboard.*




class Dashboard : BaseActivity() {
    override fun getContext(): Context {
        return this@Dashboard
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        setSupportActionBar(toolbar)
        supportActionBar?.title = AppConstant.DASHBOARD
        txt_gotoshop.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                gotoShopList()
            }
        })
        txt_gotoproduct.setOnClickListener(object:View.OnClickListener{
            override fun onClick(v: View?) {
                gotoProductList()
            }

        })

        // The default Realm instance implicitly knows about all classes in the realmModuleAppExample Android Studio
        // module. This does not include the classes from the realmModuleLibraryExample AS module so a Realm using this
        // configuration would know about the following classes: { Cow, Pig, Snake, Spider }
        val defaultConfig = RealmConfiguration.Builder().build()

        // It is possible to extend the default schema by adding additional Realm modules using modules(). This can
        // also be Realm modules from libraries. The below Realm contains the following classes: { Cow, Pig, Snake,
        // Spider, Cat, Dog }
        @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
        val farmAnimalsConfig = RealmConfiguration.Builder()
                .name("farm.realm")
                .modules(Realm.getDefaultModule(), DomesticAnimalsModule())
                .build()
        // Or you can completely replace the default schema.
        // This Realm contains the following classes: { Elephant, Lion, Zebra, Snake, Spider }
        val exoticAnimalsConfig = RealmConfiguration.Builder()
                .name("exotic.realm")
                .modules(ZooAnimalsModule(), CreepyAnimalsModule())
                .build()
        // Multiple Realms can be open at the same time
        showMessage("Opening multiple Realms")
        val defaultRealm = Realm.getInstance(defaultConfig)
        val farmRealm = Realm.getInstance(farmAnimalsConfig)
        val exoticRealm = Realm.getInstance(exoticAnimalsConfig)
        // Objects can be added to each Realm independantly
        showMessage("Create objects in the default Realm");
        defaultRealm.executeTransaction {realm->
            realm.createObject(Cow::class.java)
            realm.createObject(Pig::class.java)
            realm.createObject(Snake::class.java)
            realm.createObject(Spider::class.java)
        }

        showMessage("Create objects in the farm Realm");
        farmRealm.executeTransaction { realm ->
            realm.createObject(Cow::class.java)
            realm.createObject(Pig::class.java)
            realm.createObject(Cat::class.java)
            realm.createObject(Dog::class.java)
        }
        showMessage("Create objects in the exotic Realm");
        exoticRealm.executeTransaction{realm->
            realm.createObject(Elephant::class.java)
            realm.createObject(Lion::class.java)
            realm.createObject(Zebra::class.java)
            realm.createObject(Snake::class.java)
            realm.createObject(Spider::class.java)
        }

        showMessage("Copy objects between Realms")
        showMessage("Number of pigs on the farm : " + farmRealm.where(Pig::class.java).count())
        showMessage("Copy pig from defaultRealm to farmRealm")
        val defaultPig = defaultRealm.where(Pig::class.java).findFirst()
        farmRealm.executeTransaction{realm->
            realm.copyToRealm(defaultPig);
        }

        showMessage("Number of unnamed Dog on the farm : " +
                farmRealm.where(Dog::class.java).isNull("name").count())
        // Each Realm is restricted to only accept the classes in their schema.
        showMessage("Trying to add an unsupported class");
        defaultRealm.beginTransaction();
        try {
            defaultRealm.createObject(Elephant::class.java)
        } catch (expected: RealmException) {
            showMessage("This throws a :" + expected.toString())
        } finally {
            defaultRealm.cancelTransaction()
        }

        // And Realms in library projects are independent from Realms in the app code
        showMessage("Interacting with library code that uses Realm internally")
        val animals = 5
        val libraryZoo = Zoo()
        libraryZoo.open()
        showMessage("Adding animals: " + animals)
        libraryZoo.addAnimals(5)
        showMessage("Number of animals in the library Realm:" + libraryZoo.noOfAnimals)
        libraryZoo.close()

        // Remember to close all open Realms
        defaultRealm.close();
        farmRealm.close();
        exoticRealm.close();
    }

    private fun gotoProductList() {
        val prodListIntent=Intent(getContext(), ProductListActivity::class.java)
        startActivity(prodListIntent)
    }

    private fun gotoShopList() {
        val shoplistIntent= Intent(getContext(), ShopListActivity::class.java)
        startActivity(shoplistIntent)
    }
}
