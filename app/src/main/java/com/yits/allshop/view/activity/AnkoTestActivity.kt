package com.yits.allshop.view.activity

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import com.yits.allshop.R
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class AnkoTestActivity : AppCompatActivity() {
    var context: Context?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context=this@AnkoTestActivity
       AnkoTestActivityUI().setContentView(this)
    }

    class AnkoTestActivityUI():AnkoComponent<AnkoTestActivity>
    {

        override fun createView(ui: AnkoContext<AnkoTestActivity>)= with(ui) {

            verticalLayout{
               backgroundColor=R.color.greay
                gravity=Gravity.CENTER
                padding=dip(20)
                textView{
                    gravity=Gravity.CENTER
                    text="Enter your request"
                    textSize=20f
                }.lparams(width= matchParent){
                    bottomMargin=dip(10)
                    topMargin=dip(10)
                    leftMargin=dip(50)
                    rightMargin=dip(50)
                }

                val name=editText{
                    hint="What is your name?"
                }

                val message=editText{
                    hint="What is your message?"
                    lines=1
                }

                button("Enter"){
                    textSize=20f
                    onClick {
                        toast("Hey ${name.text}! You wrote ${message.text}.Thank you for contacting us.We will" +
                                "touch with you soon.")
                    }
                }.lparams(wrapContent,dip(50)){
                    gravity=Gravity.RIGHT
                    rightMargin=dip(5)
                }
            }
        }

    }
}
