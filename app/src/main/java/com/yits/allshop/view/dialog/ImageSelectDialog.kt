package com.yits.allshop.view.dialog

import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatDialog
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.yits.allshop.R
import com.yits.allshop.presenter.ICaptureImageItemClickListener
import com.yits.allshop.utility.FontType
import kotlinx.android.synthetic.main.camera_gallery_layout.*
import kotlinx.android.synthetic.main.imagecaptureoption_dialog.*

/**
 * Created by yitsol on 23-02-2018.
 */
class ImageSelectDialog(context: Context?) : AppCompatDialog(context), View.OnClickListener {


    private var clickListener: ICaptureImageItemClickListener? = null
    private var _context: Context?=null

    init {
        this._context=context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window!!.attributes.windowAnimations = R.style.OSFilterDialogTheme
        this.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setContentView(R.layout.imagecaptureoption_dialog)
        setAppFontType(ll_addsnap)
        txt_camera.setOnClickListener(this)
        txt_gallery.setOnClickListener(this)
        img_appcamera.setOnClickListener(this)
        img_appgallery.setOnClickListener(this)
        txt_cancel.setOnClickListener(this)
    }

    private fun setAppFontType(viewGroup: ViewGroup) {
        FontType(_context, viewGroup)
    }

    fun setDialogItemSelectListner(listner: ICaptureImageItemClickListener) {
        this.clickListener = listner
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun setOnDismissListener(listener: DialogInterface.OnDismissListener?) {
        super.setOnDismissListener(listener)
        clickListener=null
    }

    override fun setOnShowListener(listener: DialogInterface.OnShowListener?) {
        super.setOnShowListener(listener)
    }

    override fun setOnCancelListener(listener: DialogInterface.OnCancelListener?) {
        super.setOnCancelListener(listener)
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.txt_camera->{
                dismiss()
                clickListener?.onCaptureImgCamera()
            }
            R.id.txt_gallery->{
                dismiss()
                clickListener?.onCaptureImgGallery()}
            R.id.txt_cancel->this.dismiss()
        }
    }


}