package com.yits.allshop.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import com.yits.allshop.R
import com.yits.allshop.adapter.ShopDefImgAdapter
import com.yits.allshop.adapter.ShopTypeAdapter
import com.yits.allshop.model.AllShopDefImgModel
import com.yits.allshop.model.AllShopType
import com.yits.allshop.model.NewShopInputModel
import com.yits.allshop.presenter.IAddShopPresenter
import com.yits.allshop.presenter.IDefImgClickListener
import com.yits.allshop.presenter.IShopTypeItemListener
import com.yits.allshop.presenter.impl.AddShopPresenter
import com.yits.allshop.utility.AppConstant
import com.yits.allshop.utility.LinearLayoutSpaceItemDecoration
import com.yits.allshop.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_add_shop.*

class AddShopActivity : BaseActivity() {

    override fun getContext(): Context {
        return this@AddShopActivity
    }

    private var _presenter: IAddShopPresenter? = null
    private var _shopTypeAdapter: ShopTypeAdapter? = null
    private var _shopDefImgAdaper: ShopDefImgAdapter? = null
    private var shopTypeId:String="NA"
    private var shopTypeName:String="NA"
    private var shopImgUrl:String="NA"
    private var inputmodel:NewShopInputModel?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_shop)
        setSupportActionBar(toolbar)
        supportActionBar?.title = AppConstant.ADDNEWSHOP
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        _presenter = AddShopPresenter(this)
        btn_saveShop.setOnClickListener(View.OnClickListener {
            _presenter?.saveNewShop(edt_shopName,edt_shopOwnerName,edt_shopAddrs
            ,edt_shopPhone,shopTypeId,shopTypeName,shopImgUrl)
        })
    }

    override fun onStart() {
        super.onStart()
        _presenter?.subscribeCallbacks()
    }

    override fun onStop() {
        super.onStop()
        _presenter?.unSubscribeCallbacks()
    }

    override fun onResume() {
        super.onResume()
        _presenter?.getAllShopType()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> gotobackScreen()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun gotobackScreen() {
        val shoplistIntent= Intent(getContext(), ShopListActivity::class.java)
        startActivity(shoplistIntent)
    }

    fun showDiffShopType(allShopType: AllShopType) {
        rcly_shopTypList.layoutManager = LinearLayoutManager(getActContext(), LinearLayout.HORIZONTAL, false)
        rcly_shopTypList.addItemDecoration(LinearLayoutSpaceItemDecoration(5))
        _shopTypeAdapter = ShopTypeAdapter(getActContext(), allShopType.data)
        rcly_shopTypList.adapter = _shopTypeAdapter
        _shopTypeAdapter?.registerItemClickListener(object:IShopTypeItemListener
        {
            override fun onShopTypeClick(id: String, typeId: String, typeName: String) {
                shopTypeId=id
                shopTypeName=typeName
            }
        })
        _presenter?.getAllShopImage()
    }

    fun showAllDefShopImg(defshopimglist: List<AllShopDefImgModel>) {
        rcly_shopImgList.layoutManager = LinearLayoutManager(getActContext(), LinearLayout.HORIZONTAL, false)
        rcly_shopImgList.addItemDecoration(LinearLayoutSpaceItemDecoration(5))
        _shopDefImgAdaper = ShopDefImgAdapter(getActContext(), defshopimglist)
        rcly_shopImgList.adapter = _shopDefImgAdaper
        _shopDefImgAdaper?.registerClickListener(object :IDefImgClickListener{
            override fun OnDefImgClickListner(index: Int, ImgUrl: String) {
                shopImgUrl=ImgUrl
            }
        })
    }

    fun saveShopComplete()
    {
        showMessage("Shop detail save successfully")
        gotobackScreen()
    }
}
