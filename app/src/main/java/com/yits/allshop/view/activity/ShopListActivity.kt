package com.yits.allshop.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import com.yits.allshop.R
import com.yits.allshop.adapter.ShopListAdapter
import com.yits.allshop.model.ShopDataModel
import com.yits.allshop.presenter.IShopListItemClickListener
import com.yits.allshop.presenter.IShopListPresenter
import com.yits.allshop.presenter.impl.ShopListPresenter
import com.yits.allshop.utility.AppConstant
import com.yits.allshop.utility.LinearLayoutSpaceItemDecoration
import com.yits.allshop.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*

class ShopListActivity : BaseActivity() {

    private var _presenter:IShopListPresenter?=null
    private var _shopListAdapter: ShopListAdapter?=null


    override fun getContext(): Context {
        return this@ShopListActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar?.title=AppConstant.ALLSHOPLIST
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        _presenter=ShopListPresenter(this)
        txt_addnewshop.setOnClickListener(View.OnClickListener {
            gotoAddShopAct()
        })
    }

    override fun onStart() {
        super.onStart()
        _presenter?.subscribeCallbacks()
    }

    override fun onStop() {
        super.onStop()
        _presenter?.unSubscribeCallbacks()
    }

    override fun onResume() {
        super.onResume()
        _presenter?.getAllShopDetails()
    }

    private fun gotoAddShopAct() {
        val addshopIntent=Intent(getActContext(), AddShopActivity::class.java)
        startActivity(addshopIntent)
    }

    fun showAllShopDetails(shopList:List<ShopDataModel>)
    {
        rcly_allshoplist.layoutManager=LinearLayoutManager(getContext(),LinearLayout.VERTICAL,false)
        rcly_allshoplist.addItemDecoration(LinearLayoutSpaceItemDecoration(5))
        _shopListAdapter= ShopListAdapter(getContext(),shopList)
        rcly_allshoplist.adapter=_shopListAdapter
        _shopListAdapter?.registerListener(object:IShopListItemClickListener{
            override fun onItemEditClick(position: Int, datamodel: ShopDataModel) {
                gotoEditShopScreen(datamodel)
            }

            override fun onItemClick(position: Int, datamodel: ShopDataModel) {
                _presenter!!.deleteShopFromPosition(datamodel)
            }
        })
    }

    private fun gotoEditShopScreen(datamodel: ShopDataModel) {
        val editIntent=Intent(getContext(), EditShopActivity::class.java)
        editIntent.putExtra(AppConstant.SHOPID,datamodel.id)
        startActivity(editIntent)
    }

    fun shopDeleteSuccess()
    {
        _presenter?.getAllShopDetails()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean
    {
        when(item?.itemId)
        {
            android.R.id.home -> gotobackScreen()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun gotobackScreen() {
        val homeIntent=Intent(getContext(),Dashboard::class.java)
        startActivity(homeIntent)
    }
}
