package com.yits.allshop.view.base

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.yits.allshop.R

abstract class BaseActivity : AppCompatActivity() {

    private var dialog: Dialog? = null
    private var animationDrawable: AnimationDrawable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun showMessage(message: String) {
        Toast.makeText(getActContext(), message, Toast.LENGTH_SHORT).show()
    }

    abstract fun getContext(): Context

    fun getActContext(): BaseActivity {
        return this@BaseActivity
    }

    internal inner class Runloader(private val strrMsg: String) : Runnable {
        @SuppressLint("ResourceType")
        override fun run() {
            try {
                if (dialog == null) {
                    dialog = Dialog(getActContext(), R.style.Theme_Dialog_Translucent_Dialog)
                    dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
                    dialog!!.getWindow()!!.setBackgroundDrawable(
                            ColorDrawable(
                                    Color.TRANSPARENT))
                }
                dialog!!.setContentView(R.layout.loading)
                dialog!!.setCancelable(false)
                if (dialog != null && dialog!!.isShowing()) {
                    dialog!!.dismiss()
                    dialog = null
                }
                if (dialog != null) {
                    dialog!!.show()
                }
                var imgeView: ImageView? = null
                if (dialog != null) {
                    imgeView = dialog!!.findViewById(R.id.imgeView)
                    val tvLoading = dialog!!.findViewById(R.id.tvLoading) as TextView
                    if (!strrMsg.equals("", ignoreCase = true))
                        tvLoading.text = strrMsg
                    imgeView!!.setBackgroundResource(R.anim.frame)
                    animationDrawable = imgeView.background as AnimationDrawable
                    imgeView.post {
                        if (animationDrawable != null)
                            animationDrawable!!.start()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun hideloader() {
        runOnUiThread(Runnable {
            try {
                if (dialog != null && dialog!!.isShowing())
                    dialog!!.dismiss()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    fun showLoaderNew() {
        runOnUiThread(Runloader(getResources().getString(R.string.loading)))
    }

    fun showLoaderNew(msg: String) {
        runOnUiThread(Runloader(msg))
    }
}
