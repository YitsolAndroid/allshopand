package com.yits.allshop.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.widget.LinearLayout
import com.yits.allshop.R
import com.yits.allshop.adapter.ShopDefImgAdapter
import com.yits.allshop.adapter.ShopTypeAdapter
import com.yits.allshop.model.*
import com.yits.allshop.presenter.IAddShopPresenter
import com.yits.allshop.presenter.IDefImgClickListener
import com.yits.allshop.presenter.IEditShopPresenter
import com.yits.allshop.presenter.IShopTypeItemListener
import com.yits.allshop.presenter.impl.AddShopPresenter
import com.yits.allshop.presenter.impl.EditShopPresenter
import com.yits.allshop.utility.AppConstant
import com.yits.allshop.utility.LinearLayoutSpaceItemDecoration
import com.yits.allshop.utility.LoggerUtils
import com.yits.allshop.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_edit_shop.*

class EditShopActivity : BaseActivity() {

    override fun getContext(): Context {
        return this@EditShopActivity
    }

    private var shopId:String?=null
    private var _presenter:IEditShopPresenter?=null
    private var _addShopPresenter:IAddShopPresenter?=null
    private var selectShopModel:ShopDataModel?=null
    private var _shopTypeAdapter: ShopTypeAdapter? = null
    private var _shopDefImgAdaper: ShopDefImgAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_shop)
        setSupportActionBar(toolbar)
        supportActionBar?.title = AppConstant.ADDNEWSHOP
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        val extras=intent.extras?:return
        shopId=extras.getString(AppConstant.SHOPID)
        LoggerUtils.error(EditShopActivity::class.simpleName,"select shop id "+shopId)
        _presenter=EditShopPresenter(this)
        _addShopPresenter=AddShopPresenter(this)
    }

    override fun onStart() {
        super.onStart()
        _presenter?.subscribeCallbacks()
        _addShopPresenter?.subscribeCallbacks()
    }

    override fun onStop() {
        super.onStop()
        _presenter?.unSubscribeCallbacks()
        _addShopPresenter?.unSubscribeCallbacks()
    }

    override fun onResume() {
        super.onResume()
        shopId?.let { _presenter?.getShopDetailsById(it) }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> gotobackScreen()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun gotobackScreen() {
        val listIntent= Intent(getContext(), ShopListActivity::class.java)
        startActivity(listIntent)
    }

    fun shopdetailById(detailsModel: ShopDetailsModel)
    {
        for(datamodel: ShopDataModel in detailsModel.data!!)
        {
            selectShopModel=datamodel
            edt_shopName.setText(datamodel.shopName)
            edt_shopOwnerName.setText(datamodel.shopOwnerName)
            edt_shopAddrs.setText(datamodel.shopAddress)
            edt_shopPhone.setText(datamodel.shopPhone)
        }
        _addShopPresenter?.getAllShopType()
    }

    fun showDiffShopType(allShopType: AllShopType) {
        updateUserShopTypeSelection(selectShopModel,allShopType.data)
        rcly_shopTypList.layoutManager= LinearLayoutManager(getActContext(), LinearLayout.HORIZONTAL, false)
        rcly_shopTypList.addItemDecoration(LinearLayoutSpaceItemDecoration(5))
        _shopTypeAdapter = ShopTypeAdapter(getActContext(), allShopType.data)
        rcly_shopTypList.adapter = _shopTypeAdapter
        _shopTypeAdapter?.registerItemClickListener(object : IShopTypeItemListener{
            override fun onShopTypeClick(id: String, typeId: String, typeName: String) {

            }
        })
        _addShopPresenter?.getAllShopImage()
    }

    private fun updateUserShopTypeSelection(selectShopModel: ShopDataModel?, data: List<ShopTypeDataModel>) {
        for(typeDataModel in data)
        {
            if(typeDataModel.id.equals(selectShopModel?.shopTypeId))
            {
                typeDataModel.selectStatus=true
            }
        }
    }

    fun showAllDefShopImg(defshopimglist:List<AllShopDefImgModel>)
    {
        updateUserShopImgSelection(defshopimglist,selectShopModel)
        rcly_shopImgList.layoutManager=LinearLayoutManager(getActContext(), LinearLayout.HORIZONTAL, false)
        rcly_shopImgList.addItemDecoration(LinearLayoutSpaceItemDecoration(5))
        _shopDefImgAdaper = ShopDefImgAdapter(getActContext(), defshopimglist)
        rcly_shopImgList.adapter=_shopDefImgAdaper
        _shopDefImgAdaper?.registerClickListener(object:IDefImgClickListener
        {
            override fun OnDefImgClickListner(index: Int, ImgUrl: String) {

            }
        })
    }

    private fun updateUserShopImgSelection(defshopimglist: List<AllShopDefImgModel>, selectShopModel: ShopDataModel?) {
        when (selectShopModel?.shopType)
        {
            "Bakery"->changeColorSelection(1,defshopimglist)
            "Chicken"->changeColorSelection(2,defshopimglist)
            "Garment"->changeColorSelection(3,defshopimglist)
            "Grocery"->changeColorSelection(4,defshopimglist)
            "Jewellery"->changeColorSelection(5,defshopimglist)
            "Medical"->changeColorSelection(6,defshopimglist)
            "FruitVeg"->changeColorSelection(7,defshopimglist)
        }
    }

    private fun changeColorSelection(position: Int, defshopimglist: List<AllShopDefImgModel>) {
        for(defImgModel in defshopimglist)
        {
            if(defImgModel.index==position)
            {
                defImgModel.selectStatus=true
            }
        }
    }
}
