package com.yits.allshop.view.activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.MenuItem
import com.yits.allshop.R
import com.yits.allshop.model.AppProductImgModel
import com.yits.allshop.presenter.IAddProductPresenter
import com.yits.allshop.presenter.ICaptureImageItemClickListener
import com.yits.allshop.presenter.impl.AddProductPresenter
import com.yits.allshop.utility.AppConstant
import com.yits.allshop.view.base.BaseActivity
import com.yits.allshop.view.dialog.ImageSelectDialog
import kotlinx.android.synthetic.main.activity_add_product.*

class AddProductActivity : BaseActivity() {
    internal val PERMISSION_REQUEST_CODE_IMGUPLOAD = 105
    internal val REQUEST_PERMISSION_SETTING = 102
    private val SELECT_IMAGE_FROM_GALLERY = 1000
    var imgSelectDialog: ImageSelectDialog?=null
    var _presenter:IAddProductPresenter?=null
    override fun getContext(): Context {
        return this@AddProductActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_product)
        setSupportActionBar(toolbar)
        supportActionBar?.title = AppConstant.ADDNEWPRODUCT
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        _presenter=AddProductPresenter(this)
        img_captureProdImg.setOnClickListener({
            _presenter?.getAllProductImgUrlDetails()
            //pickImageForProduct()
        })
    }

    override fun onStart() {
        super.onStart()
        _presenter?.subscribeCallbacks()
    }

    override fun onStop() {
        super.onStop()
        _presenter?.unSubscribeCallbacks()
    }

    override fun onResume() {
        super.onResume()

    }

    private fun pickImageForProduct() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkPermisson()) {
                showImageCapturDialog()
            }
            else
            {
                requestPermission()
            }
        }
    }

    private fun showImageCapturDialog() {
        imgSelectDialog= ImageSelectDialog(getContext())
        imgSelectDialog?.setDialogItemSelectListner(object: ICaptureImageItemClickListener{
            override fun onCaptureImgCamera() {

            }

            override fun onCaptureImgGallery() {
                openGalleryForImage()
            }

        })
    }

    private fun openGalleryForImage() {
        val galleryIntent=Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        galleryIntent.setType("image/*")
        startActivityForResult(Intent.createChooser(galleryIntent,"Select File"),SELECT_IMAGE_FROM_GALLERY)
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        ,PERMISSION_REQUEST_CODE_IMGUPLOAD)
    }

    fun checkPermisson():Boolean
    {
        val result = ContextCompat.checkSelfPermission(getContext(),Manifest.permission.READ_EXTERNAL_STORAGE)
        return result==PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode)
        {
            PERMISSION_REQUEST_CODE_IMGUPLOAD->showImageCapturePermissionDialog(grantResults)
        }
    }

    private fun showImageCapturePermissionDialog(grantResults: IntArray) {
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            showImageCapturDialog()
        } else if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED ||
                grantResults[1] == PackageManager.PERMISSION_DENIED) {
            showMessage("Permission denied to use Camera")
            if (Build.VERSION.SDK_INT >= 23 && !shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showAppAlertDialog("Alert", "Looks like you have not provided storage permission for using the camera, " + "Enable it from settings to use the camera.", true, "GO TO SETTINGS")
            }

        }
    }

    fun showAppAlertDialog(title: String, message: String, dialogcancelable: Boolean, positiveText: String) {
        android.app.AlertDialog.Builder(getContext())
                .setTitle(title)
                .setMessage(message)
                .setCancelable(dialogcancelable)
                .setPositiveButton(positiveText) { dialogInterface, i -> gotoRequireSetting() }.show()
    }

    private fun gotoRequireSetting() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", getContext().getPackageName(), null)
        intent.data = uri
        startActivityForResult(intent, REQUEST_PERMISSION_SETTING)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //super.onActivityResult(requestCode, resultCode, data)
        when(requestCode)
        {
            REQUEST_PERMISSION_SETTING->pickImageForProduct()
            SELECT_IMAGE_FROM_GALLERY->gallerySelectionDone(resultCode, data)
            else-> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun gallerySelectionDone(resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            val galleryImageuri = data?.getData()
        }
        else
        {
            showMessage("error while selecting image")
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId)
        {
            android.R.id.home->gobacktoScreen()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun gobacktoScreen() {
        val prodlistIntent=Intent(getContext(), ProductListActivity::class.java)
        startActivity(prodlistIntent)
    }

    fun showProductImgListDialog(productImgList:List<AppProductImgModel>)
    {

    }

}
