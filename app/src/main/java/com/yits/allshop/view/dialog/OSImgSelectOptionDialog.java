package com.yits.allshop.view.dialog;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialog;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yits.allshop.R;
import com.yits.allshop.presenter.ICaptureImageItemClickListener;
import com.yits.allshop.utility.FontType;


/**
 * Created by user on 5/18/2017.
 */

public class OSImgSelectOptionDialog extends AppCompatDialog implements View.OnClickListener {
    private ICaptureImageItemClickListener clickListener;
    private TextView txt_camera,txt_gallery,txt_cancel;
    private ImageView img_appcamera,img_appgallery;
    LinearLayout ll_addsnap,ll_cameragallery;
    Context _context;

    public OSImgSelectOptionDialog(Context context) {
        super(context);
        this._context=context;
    }

    public OSImgSelectOptionDialog(Context context, int theme) {
        super(context, theme);
    }

    protected OSImgSelectOptionDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().getAttributes().windowAnimations= R.style.OSFilterDialogTheme;
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.imagecaptureoption_dialog);
        initView();
        setAppFontType(ll_addsnap);
    }

    private void setImgViewHeightAndWidth(int value)
    {
        img_appcamera.getLayoutParams().height=value;
        img_appcamera.getLayoutParams().width=value;
        img_appgallery.getLayoutParams().height=value;
        img_appgallery.getLayoutParams().width=value;
        img_appcamera.requestLayout();
        img_appgallery.requestLayout();
    }

    public void setAppFontType(ViewGroup viewGroup)
    {
        new FontType(_context,viewGroup);
    }


    private void initView() {
        ll_addsnap=(LinearLayout)findViewById(R.id.ll_addsnap);
        txt_camera= (TextView) findViewById(R.id.txt_camera);
        txt_gallery= (TextView) findViewById(R.id.txt_gallery);
        txt_cancel= (TextView) findViewById(R.id.txt_cancel);
        img_appcamera= (ImageView) findViewById(R.id.img_appcamera);
        img_appgallery= (ImageView) findViewById(R.id.img_appgallery);
        ll_cameragallery= (LinearLayout) findViewById(R.id.ll_cameragallery);
        txt_camera.setOnClickListener(this);
        txt_gallery.setOnClickListener(this);
        txt_cancel.setOnClickListener(this);
        img_appcamera.setOnClickListener(this);
        img_appgallery.setOnClickListener(this);
    }

    public void setDialogItemSelectListner(ICaptureImageItemClickListener listner)
    {
        this.clickListener=listner;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void setOnDismissListener(@Nullable OnDismissListener listener) {
        super.setOnDismissListener(listener);
        clickListener=null;
    }

    @Override
    public void setOnShowListener(@Nullable OnShowListener listener) {
        super.setOnShowListener(listener);
    }

    @Override
    public void setOnCancelListener(@Nullable OnCancelListener listener) {
        super.setOnCancelListener(listener);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txt_camera:
                dismiss();
                clickListener.onCaptureImgCamera();
                break;
            case R.id.txt_gallery:
                dismiss();
                clickListener.onCaptureImgGallery();
                break;
            case R.id.txt_cancel:
                dismiss();
                break;
            case R.id.img_appcamera:
                dismiss();
                clickListener.onCaptureImgCamera();
                break;
            case R.id.img_appgallery:
                dismiss();
                clickListener.onCaptureImgGallery();
                break;
        }
    }
}
